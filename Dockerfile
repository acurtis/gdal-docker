FROM centos:7 as builder

RUN set -x && \
	yum install -y epel-release centos-release-scl \
	yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm && \
	yum update -y && \
	yum install -y \
		java-1.8.0-openjdk java-1.8.0-openjdk-devel unzip dejavu-fonts postgresql11 libcurl wget openssl libxml2  \
		python36 python36-libs python36-numpy python36-devel python36-setuptools python36-pip && \
	pip3.6 install --upgrade pip && \
	yum -y clean all

ARG GDAL_VERSION
ARG SQLITE_VERSION
ARG MRSID_VERSION
ARG OPENJPEG_VERSION
ARG GEOS_VERSION
ARG PROJ_VERSION
ARG HDF5_VERSION

RUN set -x && \
    yum groupinstall -y "Development Tools" && \
    yum install -y postgresql11-devel libcurl-devel autoconf automake libtool which openssl-devel libxml2-devel ant cmake

COPY assets/blobs/sqlite-${SQLITE_VERSION}.tar.gz /workspace/archive/
COPY assets/blobs/gdal-${GDAL_VERSION}.tar.gz /workspace/archive/
COPY assets/blobs/MrSID_DSDK-${MRSID_VERSION}.tar.gz /workspace/archive/
COPY assets/blobs/openjpeg-${OPENJPEG_VERSION}.tar.gz /workspace/archive/
COPY assets/blobs/geos-${GEOS_VERSION}.tar.gz /workspace/archive/
COPY assets/blobs/hdf5-${HDF5_VERSION}.tar.gz /workspace/archive/
COPY assets/blobs/proj-${PROJ_VERSION}.tar.gz /workspace/archive/
COPY assets/build_gdal.sh /workspace/

RUN /workspace/build_gdal.sh
ENV PATH="/opt/gdal/bin:${PATH}" 
CMD [ "/bin/bash" ]

FROM centos:7 as gdal

RUN set -x && \
	yum install -y epel-release centos-release-scl \
	yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm && \
	yum update -y && \
	yum install -y \
		java-1.8.0-openjdk java-1.8.0-openjdk-devel unzip dejavu-fonts postgresql11 libcurl wget openssl libxml2  \
		python36 python36-libs python36-numpy python36-devel python36-setuptools python36-pip && \
	pip3.6 install --upgrade pip && \
	yum -y clean all

ENV PATH="/opt/gdal/bin:${PATH}" 

COPY --from=builder /opt/gdal /opt/gdal

RUN set -x && \
	echo "/opt/gdal/lib/" > /etc/ld.so.conf.d/gdal-lib.conf && \
	/usr/sbin/ldconfig -v && \
	echo "/opt/gdal/lib64/python3.6/site-packages" > /usr/lib64/python3.6/site-packages/osgeo.pth

ENV PATH="/opt/gdal/bin:${PATH}" 

CMD [ "/bin/bash" ]
