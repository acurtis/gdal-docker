#! /bin/bash -x

. ./env

mkdir -p assets/blobs/plugins
echo "Fetching GDAL Source (${GDAL_VERSION})"
curl -s -L -o assets/blobs/sqlite-${SQLITE_VERSION}.tar.gz -z assets/blobs/sqlite-${SQLITE_VERSION}.tar.gz https://www.sqlite.org/2020/sqlite-autoconf-${SQLITE_VERSION}.tar.gz
curl -s -L -o assets/blobs/gdal-${GDAL_VERSION}.tar.gz -z assets/blobs/gdal-${GDAL_VERSION}.tar.gz https://github.com/OSGeo/gdal/releases/download/v${GDAL_VERSION}/gdal-${GDAL_VERSION}.tar.gz
curl -s -L -o assets/blobs/MrSID_DSDK-${MRSID_VERSION}.tar.gz -z assets/blobs/MrSID_DSDK-${MRSID_VERSION}.tar.gz http://bin.lizardtech.com/download/developer/MrSID_DSDK-${MRSID_VERSION}.tar.gz
curl -s -L -o assets/blobs/openjpeg-${OPENJPEG_VERSION}.tar.gz -z assets/blobs/openjpeg-${OPENJPEG_VERSION}.tar.gz https://github.com/uclouvain/openjpeg/archive/v${OPENJPEG_VERSION}.tar.gz
curl -s -L -o assets/blobs/proj-${PROJ_VERSION}.tar.gz -z assets/blobs/proj-${PROJ_VERSION}.tar.gz https://github.com/OSGeo/PROJ/releases/download/${PROJ_VERSION}/proj-${PROJ_VERSION}.tar.gz
curl -s -L -o assets/blobs/geos-${GEOS_VERSION}.tar.gz -z assets/blobs/geos-${GEOS_VERSION}.tar.gz https://github.com/libgeos/geos/archive/${GEOS_VERSION}.tar.gz
curl -s -L -o assets/blobs/hdf5-${HDF5_VERSION}.tar.gz -z assets/blobs/hdf5-${HDF5_VERSION}.tar.gz https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-$(echo ${HDF5_VERSION} | sed 's/\.[0-9]$//')/hdf5-${HDF5_VERSION}/src/hdf5-${HDF5_VERSION}.tar.gz

BUILD_ARGS=""
while read e; do
    BUILD_ARGS="${BUILD_ARGS} --build-arg $e";
done <env

echo $BUILD_ARGS
docker build --target builder -t local/gdal-builder:${GDAL_VERSION} ${BUILD_ARGS} .
docker build --rm -t local/gdal:${GDAL_VERSION} ${BUILD_ARGS} .
docker tag local/gdal:${GDAL_VERSION} local/gdal:latest 
