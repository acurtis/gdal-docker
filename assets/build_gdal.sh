#! /bin/bash -x
env
mkdir -p /opt/gdal/include /opt/gdal/lib /opt/gdal/bin
# echo "Installing MrSID Libraries"
# cd /workspace
# tar -xzf /workspace/archive/MrSID_DSDK-${MRSID_VERSION}.tar.gz
# cp -rp /workspace/MrSID_DSDK-${MRSID_VERSION}/Raster_DSDK/include/* /opt/gdal/include/
# cp -rp /workspace/MrSID_DSDK-${MRSID_VERSION}/Raster_DSDK/lib/* /opt/gdal/lib/
# cp -rp /workspace/MrSID_DSDK-${MRSID_VERSION}/Raster_DSDK/bin/* /opt/gdal/bin/

export PKG_CONFIG_PATH=/opt/gdal/lib/pkgconfig:/usr/pgsql-11/lib/pkgconfig

echo "Building SQLite3"
    cd /workspace
    mkdir -p /workspace/sqlite-${SQLITE_VERSION}
    tar --strip-components=1 -C /workspace/sqlite-${SQLITE_VERSION}/  -xvf /workspace/archive/sqlite-${SQLITE_VERSION}.tar.gz
    cd /workspace/sqlite-${SQLITE_VERSION}
    ./configure --disable-tcl --prefix=/opt/gdal
    make -j4
    make install
echo "Building OpenJpeg libraries"
    cd /workspace
    tar -xzf /workspace/archive/openjpeg-${OPENJPEG_VERSION}.tar.gz
    cd openjpeg-${OPENJPEG_VERSION}
    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/gdal
    make -j4
    make install
echo "Building GEOS"
    cd /workspace
    tar -xzf /workspace/archive/geos-${GEOS_VERSION}.tar.gz
    cd /workspace/geos-${GEOS_VERSION}
    ./autogen.sh
    ./configure --prefix=/opt/gdal
    make -j4
    make install
echo "Building PROJ"
    cd /workspace
    tar -xzf /workspace/archive/proj-${PROJ_VERSION}.tar.gz
    cd /workspace/proj-${PROJ_VERSION}
    ./configure --prefix=/opt/gdal
    make -j4
    make install
echo "Building HDF5"
    cd /workspace
    tar -xzf /workspace/archive/hdf5-${HDF5_VERSION}.tar.gz
    cd /workspace/hdf5-${HDF5_VERSION}
    ./configure --prefix=/opt/gdal
    make -j4
    make install
echo "Building GDAL"
    cd /workspace
    tar -xzf /workspace/archive/gdal-${GDAL_VERSION}.tar.gz
    cd gdal-${GDAL_VERSION}
    ./configure --prefix=/opt/gdal --with-pg --with-curl=/usr/bin/curl-config \
            --with-libz=internal --with-sqlite3 --with-java --with-xml2 --with-proj=/opt/gdal \
            --with-geos=/opt/gdal/bin/geos-config --with-openjpeg=/opt/gdal --with-hdf5=/opt/gdal \
            --with-python=/usr/bin/python3
    make -j4
    make install
    cd /workspace/gdal-${GDAL_VERSION}/swig/java
    make -j4
    make install
    cp gdal.jar .libs/*.so /opt/gdal/lib
    ln -s /opt/gdal/lib/libgdalalljni.so /opt/gdal/lib/libgdaljni.so
    cd /workspace/gdal-${GDAL_VERSION}/swig/python
    make
    make install
    echo "/opt/gdal/lib/" > /etc/ld.so.conf.d/gdal-lib.conf
    /usr/sbin/ldconfig -v
    echo "/opt/gdal/lib64/python3.6/site-packages" > /usr/lib64/python3.6/site-packages/osgeo.pth
    chmod 644 /usr/lib64/python3.6/site-packages/osgeo.pth
echo "gdal-${GDAL_VERSION} Build Complete"