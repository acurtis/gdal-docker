# Dockerized GDAL 3.x on CentOS 7


Run `build.sh` to build the container, it will create the docker image `local/gdal:<version>` tag it with `local/gdal:latest` as well.



### Other notes

* Exploratory work with GDAL on CentOS 8

```
dnf --enablerepo='epel-testing' --enablerepo='PowerTools' install -y gdal
```

* COG Generation 
  
```
gdal_translate ${INPUT_IMG} \
               tmp1.tif \
               -co TILED=YES -co COMPRESS=DEFLATE
gdaladdo -r average  tmp1.tif 2 4 8 16 32
gdal_translate tmp1.tif \
               img_cloudoptimized_512.tif \
               -co TILED=YES -co COMPRESS=JPEG -co PHOTOMETRIC=YCBCR -co COPY_SRC_OVERVIEWS=YES \
               -co BLOCKXSIZE=512 -co BLOCKYSIZE=512 --config GDAL_TIFF_OVR_BLOCKSIZE 512
```
